<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header');
?>
    <section class="wrapper" style="margin-top :5px">
        <h4>Tambah Data Instansi</h4>
        <div class="showback">
            <div class="form-group">
                <form method="POST" action="<?php echo base_url(); ?>data/save">
                    <label class="control-label"> Nama Instansi</label>
                    <input class="form-control" type="text" name="nama_instansi" placeholder: Na>
                    <br>
                    <label class="control-label"> KLOP</label>
                    <select class="form-control" name="klop_id">
                        <?php foreach($klop->result() as $value){ ?>
                            <option value="<?php echo $value->id; ?>"><?php echo $value->nama;?></option>
                        <?php } ?>
                    </select>
                    <br>
                    <input class="btn btn-info" type="submit" name="submit" value="Simpan">
                </form>
            </div>
        </div>
    </section>  
<?php
$this->load->view('footer');
?>