<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {

	function __Construct(){
		parent::__Construct();
        $this->load->model('Mainmodel');
        
        if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}

	public function index($params=null, $class='success')
	{
		$sql="SELECT * FROM tbl_klop k  JOIN tbl_instansi i ON i.klop_id = k.id";
		$data=$this->Mainmodel->kueri($sql);

		$this->load->view('data_instansi',compact('data','params','class'));
	}

    public function add(){
        $sql="SELECT * FROM tbl_klop ORDER BY id";
		$klop=$this->Mainmodel->kueri($sql);

		$this->load->view('add_data_instansi',compact('klop'));
    }

	public function save(){
		$nama_instansi = $this->input->post('nama_instansi');
		$klop_id = $this->input->post('klop_id');
		$data = compact('nama_instansi','klop_id');
		if($nama_instansi == "" || $nama_instansi == null){
			unset($data['nama_instansi']);
		}
		$insert = $this->Mainmodel->Insert('tbl_instansi',$data);

		if($insert){
			$this->index("Data Berhasil Ditambahkan");
		}else{
			$this->index("Data Gagal Ditambahkan",'danger');
		}

    }
    
    public function editPPG($id, $params=null){
        $sql="SELECT * FROM tbl_instansi i JOIN tbl_klop k ON i.klop_id = k.id LEFT JOIN tbl_file f ON i.id= f.id_instansi WHERE i.id = $id";
		$data=$this->Mainmodel->kueri($sql);


        $this->load->view('editppg', compact('data','params'));
	}
}
