<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Sistem Informasi Program Pengendalian Gratifikasi</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style-responsive.css" rel="stylesheet">

    <script src="<?php echo base_url();?>assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b>Sistem Informasi Program Pengendalian Gratifikasi</b></a>
            <!--logo end-->
            
            <!--NDA-NOTIF AND INBOX-->
                       
            <div class="top-menu">
              <ul class="nav pull-right top-menu">
                  <?php 
                    if($this->session->userdata('status') != "login"){
                      echo '<li><a class="logout" href="'.base_url().'login">Login</a></li>';
                    }else{
                      echo '<li><a class="logout" href="'.base_url().'login/logout" style="background-color:red">Logout</a></li>';
                    }
                  
                  ?>
              </ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="#"><img src="<?php echo base_url();?>assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
                    
                  <li class="mt">
                      <a class="active" href="<?php echo base_url();?>">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>PPG</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?php echo base_url();?>page/rangkuman">Rangkuman</a></li>
                          <li><a  href="<?php echo base_url();?>page/kementrian">Kementerian</a></li>
                          <li><a  href="<?php echo base_url();?>page/lembaga">Lembaga Pemerintah</a></li>
                          <li><a  href="<?php echo base_url();?>page/pemprov">Pemerintah Provinsi</a></li>
                          <li><a  href="<?php echo base_url();?>page/pemkabkota">Pemerintah Kab./Kota</a></li>
                          <li><a  href="<?php echo base_url();?>page/bumn">BUMN</a></li>
                          <li><a  href="<?php echo base_url();?>page/bumd">BUMD</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Jadwal Kegiatan</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="calendar.html">Kalender</a></li>
                          <li><a  href="todo_list.html">Todo List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href=#Aturan >
                          <i class="fa fa-book"></i>
                          <span>Aturan PPG</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-plus"></i>
                          <span>Input Data</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?php echo base_url();?>data">Data Instansi & PPG</a></li>  
                      </ul>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          
          <!-- Place Your Content Here -->

