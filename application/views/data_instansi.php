<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header');
?>
    <section class="wrapper" style="margin-top :5px">
        <h4>Data Instansi</h4>
        <div class="showback">
        <?php 
            if($params){

                echo '<div class="alert alert-'.$class.' alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>'.$params.'</strong>
                </div>';
            }
        ?>
        <div class="pull-right">
            <a href="<?php echo base_url()?>data/add"><button class="btn btn-info" title="Tambah Data Instansi"><i class="fa fa-plus"></i></button></a>
            <br>
        </div>

        <table class="table table-advanced table-striped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Instansi</th>
                    <th>KLOP</th>
                    <th>Edit Data PPG</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 0 ;
                    foreach($data->result() as $v){
                    $no++;
                    echo '
                        <tr>
                            <td>'.$no.'</td>
                            <td>'.$v->nama_instansi.'</td>
                            <td>'.$v->nama.'</td>
                            <td width="15%">
                                <a href="'.base_url().'/data/editPPG/'.$v->id.'"><button class="btn btn-success"><i class="fa fa-pencil"> </i></button></a>
                            </td>
                        </tr>
                    ';
                } ?>
            </tbody>
        </table>
    </section>  
<?php
$this->load->view('footer');
?>