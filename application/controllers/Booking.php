<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {
	function __Construct(){
		parent::__Construct();
		$this->load->model('Mainmodel');
	}
	
	function package($ID){		
		$this->booking($ID);
	}
	
	function index(){
		if($this->input->post('action')=='getpesanan'){
			$this->getpesanan();
			exit;
		} else if($this->input->post('action')=='verify'){
			$this->verifydata();
			exit;
		}else if($this->input->post('action')=='verifydate'){
			$this->verifydate();
			exit;
		} else {
			$this->booking();
		}
	}
	
	private function verifydata(){
		$status='ok';
		$pesan='';
		if($this->input->post('email')){
			if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
				$status='failed';
				if($pesan) $pesan.="\n";
				$pesan.='Format email salah.';
			}
		} else {
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Email harus diisi.';
		}
		if($this->input->post('package') == ""){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Paket harus dipilih.';
		}
		if($this->input->post('baterai') <= 0){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Input Data Tambah Baterai Salah.';
		}
		if(!$this->input->post('tanggal') || $this->input->post('tanggal')== ""){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Tanggal Mulai Digunakan harus diisi.';
		}
		if($this->input->post('name') == ""){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Nama harus diisi.';
		}
		if($this->input->post('phone') == ""){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Nomor Hp harus diisi.';
		}
		
		echo json_encode(compact('status','pesan','tgl1','tgl2'));
	}

	private function booking($package_id=0){
	
		$sql="SELECT * FROM tb_package WHERE 1 ";
		$tarifs=$this->Mainmodel->kueri($sql);
		
		$sql="SELECT * FROM tb_payment_type ORDER BY id";
		$banks=$this->Mainmodel->kueri($sql);
		
		$this->load->view('booking',compact('tarifs', 'banks', 'package_id'));
	}
	
	private function getpesanan(){	
		$_totalamount=0;
		$sql="SELECT * FROM tb_package WHERE ID=".$this->input->post('c');
		$que=$this->Mainmodel->kueri($sql);
		$tarifs=array();
		foreach($que->result_array() as $res){
			$tarifs=$res;
			break;
		}
	
		$paket=$tarifs['package_type'];
		$baterai=$this->input->post('j');
		$date1=date_create($this->input->post('s'));
		
		$_rentalamount = $tarifs['price'];
		$unitrental='Paket'. $paket;
		$_totalamount+=$_rentalamount;
		$rentalamount='IDR '.number_format($_rentalamount,0,',','.');
			
		$tanggal = date_format($date1,'d-F-Y');
	
		$unitbaterai=$baterai.' unit x '.number_format(50000,0,',','.');
		$_bateraifee = 50000 * $baterai;
		$_totalamount+=$_bateraifee;
		$bateraifee='IDR '.number_format($_bateraifee,0,',','.');
		$totalamount='IDR '.number_format($_totalamount,0,',','.');
		echo json_encode(compact('paket','rentalamount','unitrental','tanggal','unitbaterai','totalamount','bateraifee'));
	}

	private function ubahtanggal($date){
		$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		
		$tahun = substr($date, 0, 4);
		$bulan = substr($date, 5, 2);
		$tgl   = substr($date, 8, 2);
		
		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
		return($result);
	}
}