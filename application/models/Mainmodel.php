<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Mainmodel extends CI_Model {
		function __construct(){
			parent::__construct();
		}
		
		public function sekarang(){
			return gmdate('Y-m-d H:i:s',time()+25200);
		}
				
		public function getMyipaddress()    
		{
			if (!empty($_SERVER['HTTP_CLIENT_IP'])){ $ip=$_SERVER['HTTP_CLIENT_IP']; }
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){ $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; }
			else { $ip=$_SERVER['REMOTE_ADDR']; }
			return $ip;
		}		
				
		public function kueri($sql=''){
			if($sql) return $this->db->query($sql);
		}
		
		public function Update($table,$data,$where){
			$str_set='';
			WHILE(list($field,$value)=each($data)){
				$str_set.="`{$field}`='{$value}',";
			}
			$str_set=rtrim($str_set,',');
			$sql="UPDATE `{$table}` SET {$str_set} WHERE {$where}";
			//return $this->db->query($sql);
			$this->db->query($sql);
			return 1;
		}
		
		public function Insert($table,$data){
			$sql=$this->db->insert_string($table, $data);
			$this->db->query($sql);
			return $this->db->insert_id();
		}
		
		public function Users(){
			$sql="SELECT * FROM tb_user";
			$que=$this->db->query($sql);
			$users=array();
			$users=array();
			foreach($que->result() as $res){
				$users[$res->ID]=$res->username;	
			}
			return $users;
		}
		
		
		
	}