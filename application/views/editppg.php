<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header');
?>
    <section class="wrapper" style="margin-top :5px">
        <h4>Data PPG Instansi</h4>
        <div class="showback">
        <?php 
            if($params){

                echo '<div class="alert alert-'.$class.' alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>'.$params.'</strong>
                </div>';
            }
        ?>
        <div class="pull-right">
            <a href="<?php echo base_url()?>data/add"><button class="btn btn-info">Tambah Data Instansi</button></a>
        </div>

        <table class="table table-advanced table-striped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Instansi</th>
                    <th>KLOP</th>
                    <th>Pengenalan</th>
                    <th>Komitmen</th>
                    <th>UPG</th>
                    <th>Aturan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 0 ;
                    foreach($data->result() as $v){
                    $no++;
                    echo '
                        <tr>
                            <td>'.$no.'</td>
                            <td>'.$v->nama_instansi.'</td>
                            <td>'.$v->nama.'</td>
                            <td> status <input value="" type="checkbox"> </td>
                            <td> status <input value="" type="checkbox"> </td>
                            <td> status <input value="" type="checkbox"> </td>
                            <td> status <input value="" type="checkbox"> </td>
                            <td> status <input value="" type="checkbox"> </td>
                        </tr>
                    ';
                } ?>
            </tbody>
        </table>
    </section>  
<?php
$this->load->view('footer');
?>