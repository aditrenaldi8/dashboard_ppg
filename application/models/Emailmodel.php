<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailmodel extends CI_Model {
	function __construct(){
		parent::__construct();
	}


	function loadBodyMail($fulname='',
		$phone='',
		$email='',
		$fullAdress1='',
		$fullAdress2='',
		$orderId='',
		$unit,
		$totalDeposit=0,
		$totalAdministrasi=0,
		$hargaPromosi=0,
		$totalGrandTotal=0,
		$status,
		$keterangan,
		$keterangan2,
		$dataPesan){
		
		if($keterangan2 == 0){
			$html = $this->htmlemail();
		}else{
			$html = $this->htmlemail_add_day();
		}
		$html = str_replace('{fulname}', $fulname, $html);
		$html = str_replace('{phone}', $phone, $html);
		$html = str_replace('{email}', $email, $html);

		$html = str_replace('{fullAdress1}', $fullAdress1, $html);
		$html = str_replace('{fullAdress2}', $fullAdress2, $html);

		$html = str_replace('{orderId}', $orderId, $html);
		$html = str_replace('{unit}', $unit, $html);
		$html = str_replace('{status}', $status, $html);
		$html = str_replace('{keterangan}', $keterangan, $html);
		$tb = "";

		$tb.="<tr>";
		$tb.="<th scope='row'>1</th>";
		$tb.="<td>".$dataPesan->country."</td>";
		$tb.="<td align='center'>".date('Y-m-d',strtotime($dataPesan->start_date))."</td>";
		$tb.="<td align='center'>".date('Y-m-d',strtotime($dataPesan->end_date))."</td>";
		$tb.="<td>".$dataPesan->day_count." Hari</td>";
		$tb.="<td align='right'><span class='pull-right'>Rp. ".number_format((int)$dataPesan->rental_amount,0)."</span></td>";
		$tb.="</tr>";
		
		$tb.="<tr>";
		$tb.="<th scope='row'></th>";
		$tb.="<td colspan='4'><span class='pull-right'><strong>Deposit </strong>(Untuk semua modem dalam pemesanan tiap unit)</span></td>";
		$tb.="<td align='right'><span class='pull-right'>Rp. ".number_format((int)$dataPesan->deposit_amount,0)."</span></td>";
		$tb.="</tr>";
		$tb.="<tr>";
		$tb.="<th scope='row'></th>";
		$tb.="<td colspan='4'><span class='pull-right'>Administrasi</span></td>";
		$tb.="<td align='right'><span class='pull-right'>Rp. ".number_format((int)$dataPesan->admin_fee,0)."</span></td>";
		$tb.="</tr>";
		$tb.="<tr>";
		$tb.="<th scope='row'></th>";
		$tb.="<td colspan='4'><span class='pull-right'>Discount</span></td>";
		$tb.="<td align='right'><span class='pull-right'>Rp. ".number_format((int)$dataPesan->voucher_amount,0)."</span></td>";
		$tb.="</tr>";
		$tb.="<tr>";
		$tb.="<th scope='row'></th>";
		$tb.="<td colspan='4'><span class='pull-right'>Total</span></td>";

		$tb.="<td align='right'><span class='pull-right'><strong>Rp. ".number_format($dataPesan->total_amount,0)."</strong></span></td>";
		$tb.="</tr>";
		$html = str_replace('{detailpemesanan}', $tb, $html);

		return $html;
	}
	
	private function htmlemail(){
		return<<<HTML

<html>
	<head>
	<style>
	body {
		font-family: arial, sans-serif;
		font-size:14px;
		margin:20px;
	}
	
	</style>
	</head>
	<body>
		<span>Dear <b>{fulname}</b>,</span>
		<br/>
		<p>Terima kasih untuk pemesanan Anda di Passpod rental WiFi.</p>
		<p>Keterangan Pemesanan<br><br>
		<b>Order id:</b> {orderId}<br>
		<b>Jumlah:</b> {unit} Unit<br>
		<b>Status:</b> {status}<br><br>
		<!-- Harap segera melakukan pembayaran ke rekening kami di BCA 270 717 1819 an PT. YELOOO INTEGRA DATANET<br><br> -->
		<b>{keterangan}</b>
		<br><br>
		</p>
		<table width="100%" style="border-collapse: collapse;font-size:12px" border="1" cellpadding="4" cellspacing="2">
				  <thead>
				    <tr>
				      <th>#</th>
				      <th>Negara</th>
				      <th>Tanggal Keberangkatan</th>
				      <th>Tanggal Kepulangan</th>
				      <th>Days</th>
				      <th>Harga</th>
				    </tr>
				  </thead>
				  <tbody>
				   {detailpemesanan}
				  </tbody>
		</table>
		<br><br>
		<span><b>Informasi Pelanggan</b></span>
		<br>
		<table width="100%" style="font-size:14px">
			<tr>
				<td width="20%">Nama Pelanggan</td>
				<td width="80%">: <b>{fulname}</b></td>
			</tr>
			<tr>
				<td width="20%">Kontak Pelanggan</td>
				<td width="80%">: <b>{phone}</b></td>
			</tr>
			<tr>
				<td width="20%">Email Pelanggan</td>
				<td width="80%">: <b>{email}</b></td>
			</tr>
			
		</table>
		<br><br>
		<b>Metode Pengiriman:</b> 
		<p>{fullAdress1}</p>
		<b>Metode Pengembalian:</b> 
		<p>{fullAdress2}</p>
		<br/>
		<p>Sekali lagi, terima kasih telah memilih Passpod Travel WiFi. Jangan ragu untuk kontak kami jika membutuhkan bantuan.</p>
		<br><br>
		<b>*Note : 
			<p>1. Apabila ada informasi pemesanan yang salah, Silahkan hubungi Customer Service Kami</p>
			<p>2. Jika Pengambilan Modem dilakukan di Bandara, Silahkan Whatsapp ke nomor +62 838-921-888-33</p>
		</b>
		<br><br>

		<span>Thanks!<br/>
		The Passpod Team</span>
		<br><br>
		<img src="http://passpod.id/assets/images/logo_passpod.png" alt="Passpod.id|LAYANAN TERBAIK BUAT ANDA" width="150px">
		<br><br>
		<table width="100%" style="font-size:11px">
			<tr>
				<td>Hotline</td>
				<td>: +62 812 8 171819</td>
			</tr>
			<tr>
				<td>Whatsapp</td>
				<td>: +62 888 1 171819</td>
			</tr>
			<tr>
				<td>Fixed line</td>
				<td>: +6221 63850731</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>: support[at]passpod.id</td>
			</tr>
			<tr>
				<td>Facebook</td>
				<td><a href="https://www.facebook.com/Passpod-724533597698946/" title="FB Passpod">www.facebook.com/passpod</a></td>
			</tr>
			<tr>
				<td>Instagram</td>
				<td><a href="https://www.instagram.com/passpod_wifi/" title="Instaagram Passpod">www.instagram.com/passpod_wifi</a></td>
			</tr>
			<tr>
				<td>Alamat Kantor</td>
				<td>Jl. K.H.Hasyim Ashari Ruko Roxy Mas Blok C 2 No.37 Jakarta Pusat 10150</td>
			</tr>
		</table>
		</p>
	</body>
</html>
		
HTML;
		
	}
		
	private function htmlemail_add_day(){
		return<<<HTML
		
<html>
	<head>
	<style>
	body {
		font-family: arial, sans-serif;
		font-size:14px;
		margin:20px;
	}
				
	</style>
	</head>
	<body>
		<span>Dear <b>{fulname}</b>,</span>
		<br/>
		<p>Terima kasih untuk pemesanan Anda di Passpod rental WiFi.</p>
		<p>Keterangan Pemesanan<br><br>
		<b>Order id:</b> {orderId}<br>
		<b>Jumlah:</b> {unit} Unit<br>
		<b>Status:</b> {status}<br><br>
		<!-- Harap segera melakukan pembayaran ke rekening kami di BCA 270 717 1819 an PT. YELOOO INTEGRA DATANET<br><br> -->
		<b>{keterangan}</b>
		<br><br>
		</p>
		<table width="100%" style="border-collapse: collapse;font-size:12px" border="1" cellpadding="4" cellspacing="2">
				  <thead>
				    <tr>
				      <th>#</th>
				      <th>Negara</th>
				      <th>Tanggal Keberangkatan</th>
				      <th>Tanggal Kepulangan</th>
				      <th>Days</th>
				      <th>Harga</th>
				    </tr>
				  </thead>
				  <tbody>
				   {detailpemesanan}
				  </tbody>
		</table>
		<br><br>
		<span><b>Informasi Pelanggan</b></span>
		<br>
		<table width="100%" style="font-size:14px">
			<tr>
				<td width="20%">Nama Pelanggan</td>
				<td width="80%">: <b>{fulname}</b></td>
			</tr>
			<tr>
				<td width="20%">Kontak Pelanggan</td>
				<td width="80%">: <b>{phone}</b></td>
			</tr>
			<tr>
				<td width="20%">Email Pelanggan</td>
				<td width="80%">: <b>{email}</b></td>
			</tr>
				
		</table>
		<br>
		<b>*Note :
			<p>1. Apabila ada informasi pemesanan yang salah, Silahkan hubungi Customer Service Kami</p>
			<p>2. Jika Pengambilan Modem dilakukan di Bandara, Silahkan Whatsapp ke nomor +62 838-921-888-33</p>
		</b>
		<br><br>
				
		<span>Thanks!<br/>
		The Passpod Team</span>
		<br><br>
		<img src="http://passpod.id/assets/images/logo_passpod.png" alt="Passpod.id|LAYANAN TERBAIK BUAT ANDA" width="150px">
		<br><br>
		<table width="100%" style="font-size:11px">
			<tr>
				<td>Hotline</td>
				<td>: +62 812 8 171819</td>
			</tr>
			<tr>
				<td>Whatsapp</td>
				<td>: +62 888 1 171819</td>
			</tr>
			<tr>
				<td>Fixed line</td>
				<td>: +6221 63850731</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>: support[at]passpod.id</td>
			</tr>
			<tr>
				<td>Facebook</td>
				<td><a href="https://www.facebook.com/Passpod-724533597698946/" title="FB Passpod">www.facebook.com/passpod</a></td>
			</tr>
			<tr>
				<td>Instagram</td>
				<td><a href="https://www.instagram.com/passpod_wifi/" title="Instaagram Passpod">www.instagram.com/passpod_wifi</a></td>
			</tr>
			<tr>
				<td>Alamat Kantor</td>
				<td>Jl. K.H.Hasyim Ashari Ruko Roxy Mas Blok C 2 No.37 Jakarta Pusat 10150</td>
			</tr>
		</table>
		</p>
	</body>
</html>
				
HTML;
		
	}
		
}