<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	function __Construct(){
		parent::__Construct();
		$this->load->model('Mainmodel');
    }
    
    public function rangkuman(){
        $this->load->view('rangkuman');
    }
    
    public function kementrian(){
        $this->load->view('kementrian');
    }

    public function lembaga(){
        $this->load->view('lembaga');
    }

    public function pemprov(){
        $this->load->view('pemprov');
    }

    public function bumd(){
        $this->load->view('pemkabkota');
    }

    public function bumn(){
        $this->load->view('bumn');
    }
    
    public function pemkabkot(){
        $this->load->view('bumd');
    }
	
}